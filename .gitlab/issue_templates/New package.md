# Package description

Bug report:

Package will be included in the following
[meta-package(s)](https://gitlab.com/kalilinux/packages/kali-meta/-/blob/kali/master/debian/control):
* [ ] kali-tools-top10
* [ ] kali-linux-large
* [ ] kali-linux-everything
* [ ] kali-desktop-live
* [ ] Other:

Package category(ies) in
[kali-menu](https://gitlab.com/kalilinux/packages/kali-menu/-/blob/kali/master/menus/kali-applications.menu) (it can be None):  

Other information:  


# Checklist

* [ ] Prepare the package locally (following best practices for
  packaging). Do not forget to:
   * [ ] Add helper-script (if relevant)
   * [ ] Include .desktop (if it exists)
   * [ ] Include autopkgtest (at least a superficial autopkgtest)
   * [ ] Test watch file (uscan --report-status)
   * [ ] Apply Kali templates to the package ([./bin/configure-local](https://gitlab.com/kalilinux/tools/packaging/-/blob/main/bin/configure-local))
* [ ] Create the repository on
  [gitlab.com](https://gitlab.com/kalilinux/packages) (imperatively use the name of the
  source package found in debian/control and debian/changelog, always in lower case)
* [ ] Push your git repository on the gitlab repo (`git push --all && git
  push --tags`)
* [ ] Configure the GitLab repository
  ([./bin/configure-gitlab](https://gitlab.com/kalilinux/tools/packaging/-/blob/main/bin/configure-gitlab))
* [ ] Upload the package in kali-dev (`debrelease -S kali`)
* [ ] Check the migration into kali-rolling
* [ ] Test package: installation and basic functionalities
* [ ] Ask for external tests by bug submitter and Kali Developers
* [ ] Close the bug report on [bugs.kali.org](https://bugs.kali.org/)
  * Set `Fixed in Version` to next kali-release version (so that in
    shows in the [changelog page](https://bugs.kali.org/changelog_page.php))
  * Set `Status` to `resolved`
  * Set `Resolution` to `fixed`
* [ ] Add the package to
  [kali-meta](https://gitlab.com/kalilinux/packages/kali-meta)
* [ ] Add icon and desktop file to
  [kali-menu](https://gitlab.com/kalilinux/packages/kali-menu) (if relevant)
  * .desktop files goes in [desktop-files](https://gitlab.com/kalilinux/packages/kali-menu/-/tree/kali/master/desktop-files)
  * the SVG menu icon goes in [menu-icons/scalable/apps](https://gitlab.com/kalilinux/packages/kali-menu/-/tree/kali/master/menu-icons/scalable/apps)
* [ ] Add documentation on
  [kali.org/tools](https://gitlab.com/kalilinux/documentation/kali-tools)
* [ ] Add the package to
  [tools/upstream-watch](https://gitlab.com/kalilinux/tools/upstream-watch)
* [ ] Complete autopkgtest
